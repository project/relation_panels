<?php

/**
 * @file
 * Provides the the Entity Construction Kit Panelizer plugin.
 */

/**
 * Handles Entity Construction Kit specific functionality for Panelizer.
 */
class PanelizerEntityRelation extends PanelizerEntityDefault {

  public $supports_revisions = TRUE;
  public $entity_admin_root = 'admin/structure/relation/manage/%relation_panels_panelizer_relation_type';
  public $entity_admin_bundle = 4;
  public $views_table = 'relation';
  public $uses_page_manager = TRUE;

  /**
   * {@inheritdoc}
   */
  public function entity_access($op, $entity) {
    return relation_rules_access($op, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function entity_save($entity) {
    relation_save($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function entity_allows_revisions($entity) {
    return array(TRUE, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function settings_form(&$form, &$form_state) {
    parent::settings_form($form, $form_state);

    foreach ($this->plugin['bundles'] as $info) {
      if (!empty($info['status']) && !empty($info['view modes']['page_manager']['status'])) {
        $task = page_manager_get_task('relation_view');
        if (!empty($task['disabled'])) {
          drupal_set_message('The relation template page is currently not enabled in page manager. You must enable this for Panelizer to be able to panelize relations using the "Full page override" view mode.', 'warning');
        }

        $handler = page_manager_load_task_handler($task, '', 'relation_view_panelizer');
        if (!empty($handler->disabled)) {
          drupal_set_message('The panelizer variant on the relation template page is currently not enabled in page manager. You must enable this for Panelizer to be able to panelize relations using the "Full page override" view mode.', 'warning');
        }

        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hook_default_page_manager_handlers(&$handlers) {
    $handler = new stdClass;
    $handler->disabled = FALSE;
    $handler->api_version = 1;
    $handler->name = 'relation_view_panelizer';
    $handler->task = 'relation_view';
    $handler->subtask = '';
    $handler->handler = 'panelizer_node';
    $handler->weight = -100;
    $handler->conf = array(
      'title' => t('Relation Panelizer'),
      'context' => 'argument_entity_id:relation_1',
      'access' => array(),
    );
    $handlers['relation_view_panelizer'] = $handler;

    return $handlers;
  }

  /**
   * {@inheritdoc}
   */
  public function hook_form_alter(&$form, &$form_state, $form_id) {
    if ($form_id == 'relation_ui_type_form') {
      $entity = $form_state['build_info']['args'][0];
      $this->add_bundle_setting_form($form, $form_state, $entity->relation_type, array('relation_type'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get_entity_view_entity($build) {
    $element = '#entity';
    if (isset($build[$element])) {
      return $build[$element];
    }
  }
}
