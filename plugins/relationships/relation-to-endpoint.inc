<?php

/**
 * @file
 * Provides a relationship plugin for Relation entities (Relation to Endpoint).
 */

$plugin = array(
  'title' => t('Relation: Relation to Endpoint'),
  'description' => t('Creates an entity context from a relation to an endpoint.'),
  'context' => 'relation_panels_ctools_relationship_relation_to_entity_context',
  'get child' => 'relation_panels_ctools_relationship_relation_to_entity_get_child',
  'get children' => 'relation_panels_ctools_relationship_relation_to_entity_get_children',
);

/**
 * Plugin callback for retrieving a given plugin child.
 */
function relation_panels_ctools_relationship_relation_to_entity_get_child($plugin, $parent, $child) {
  $plugins = relation_panels_ctools_relationship_relation_to_entity_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

/**
 * Plugin callback for retrieving all plugin children.
 */
function relation_panels_ctools_relationship_relation_to_entity_get_children($parent_plugin, $parent) {
  $cid = $parent_plugin['name'] . ':' . $parent;
  $cache = &drupal_static(__FUNCTION__);
  if (!empty($cache[$cid])) {
    return $cache[$cid];
  }

  $entity_info = entity_get_info();
  $relation_types = relation_get_types();
  $plugins = array();

  foreach ($relation_types as $relation_name => $relation) {
    $bundles['source'] = _relation_panels_expand_relation_bundles($relation->source_bundles);
    $bundles['target'] = $relation->directional ? _relation_panels_expand_relation_bundles($relation->target_bundles) : $bundles['source'];

    foreach ($bundles as $endpoint => $entity_types) {
      foreach ($entity_types as $entity_type => $entity_bundles) {
        foreach ($entity_bundles as $entity_bundle) {
          $name = "$relation_name:$endpoint:$entity_type:$entity_bundle";
          $plugin_id = "$parent:$name";

          $replacements = array(
            '@relation' => $relation->label,
            '@bundle' => $entity_info[$entity_type]['bundles'][$entity_bundle]['label'],
            '@entity' => $entity_info[$entity_type]['label'],
            '@endpoint' => $endpoint,
          );

          $plugin = $parent_plugin;
          $plugin['title'] = t('From relation @relation to @endpoint @bundle (@entity)', $replacements);
          $plugin['keyword'] = $relation_name;
          $plugin['context name'] = $name;
          $plugin['name'] = $plugin_id;
          $plugin['description'] = t('Creates a @bundle (@entity) context from relation @relation (@endpoint).', $replacements);
          $plugin['relation'] = $relation_name;
          $plugin['parent'] = $parent;
          $plugin['required context'] = new ctools_context_required(t('Relation'), array('relation', $relation_name));

          $plugins[$plugin_id] = $plugin;
        }
      }
    }
  }

  $cache[$cid] = $plugins;
  return $plugins;
}

/**
 * Return a new context based on an existing context.
 */
function relation_panels_ctools_relationship_relation_to_entity_context($context, $configuration) {
  list(, , $endpoint, $entity_type) = explode(':', $configuration['name']);

  if (empty($context->data)) {
    return ctools_context_create_empty('entity:' . $entity_type, NULL);
  }

  $entities = field_get_items('relation', $context->data, 'endpoints');
  $index = $endpoint == 'source' ? 0 : 1;
  if (isset($entities[0]) && $entities[0]['entity_type'] == $entity_type && $entities[0]['r_index'] == $index) {
    $entity = entity_load_single($entities[0]['entity_type'], $entities[0]['entity_id']);
    return ctools_context_create('entity:' . $entity_type, $entity);
  }

  if (isset($entities[1]) && $entities[1]['entity_type'] == $entity_type && $entities[1]['r_index'] == $index) {
    $entity = entity_load_single($entities[1]['entity_type'], $entities[1]['entity_id']);
    return ctools_context_create('entity:' . $entity_type, $entity);
  }

  return ctools_context_create_empty('entity:' . $entity_type, NULL);
}
