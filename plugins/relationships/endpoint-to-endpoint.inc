<?php

/**
 * @file
 * Provides a relationship plugin for Relation entities (Endpoint to Endpoint).
 */

$plugin = array(
  'title' => t('Relation: Endpoint to Endpoint'),
  'description' => t('Creates an entity context from an endpoint to an endpoint.'),
  'context' => 'relation_panels_ctools_relationship_endpoint_to_endpoint_context',
  'get child' => 'relation_panels_ctools_relationship_endpoint_to_endpoint_get_child',
  'get children' => 'relation_panels_ctools_relationship_endpoint_to_endpoint_get_children',
);

/**
 * Plugin callback for retrieving a given plugin child.
 */
function relation_panels_ctools_relationship_endpoint_to_endpoint_get_child($plugin, $parent, $child) {
  $plugins = relation_panels_ctools_relationship_endpoint_to_endpoint_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

/**
 * Plugin callback for retrieving all plugin children.
 */
function relation_panels_ctools_relationship_endpoint_to_endpoint_get_children($parent_plugin, $parent) {
  $cid = $parent_plugin['name'] . ':' . $parent;
  $cache = &drupal_static(__FUNCTION__);
  if (!empty($cache[$cid])) {
    return $cache[$cid];
  }

  $entity_info = entity_get_info();
  $relation_types = relation_get_types();
  $plugins = array();

  foreach ($relation_types as $relation_name => $relation) {
    $source_bundles = _relation_panels_expand_relation_bundles($relation->source_bundles);
    $target_bundles = $relation->directional ? _relation_panels_expand_relation_bundles($relation->target_bundles) : $source_bundles;

    foreach ($source_bundles as $source_entity_name => $entity_source_bundles) {
      $source_entity = $entity_info[$source_entity_name];

      foreach ($target_bundles as $target_entity_name => $entity_target_bundles) {
        $target_entity = $entity_info[$target_entity_name];

        foreach ($entity_source_bundles as $source_bundle_name) {
          foreach($entity_target_bundles as $target_bundle_name) {
            if (!isset($source_entity['bundles'][$source_bundle_name]) || !isset($source_entity['bundles'][$target_bundle_name])) {
              continue;
            }

            $name = $relation_name . ':' . $source_entity_name . ':' . $source_bundle_name . ':' . $target_entity_name . ':' . $target_bundle_name . ':0';
            $plugin_id = $parent . ':' . $name;

            $replacements = array(
              '@source_entity' => $source_entity['label'],
              '@target_entity' => $target_entity['label'],
              '@source_bundle' => $source_entity['bundles'][$source_bundle_name]['label'],
              '@target_bundle' => $target_entity['bundles'][$target_bundle_name]['label'],
              '@relation' => $relation->label,
            );

            $plugin = $parent_plugin;
            $plugin['title'] = t('From @source_bundle (@source_entity) to @target_bundle (@target_entity) via relation @relation', $replacements);
            $plugin['keyword'] = $relation_name;
            $plugin['context name'] = $name;
            $plugin['name'] = $plugin_id;
            $plugin['description'] = t('Creates a @target_bundle context from @source_bundle using the @relation relation.', $replacements);
            $plugin['relation'] = $relation_name;
            $plugin['parent'] = $parent;
            $plugin['required context'] = new ctools_context_required($source_entity['label'], $source_entity_name, array('type' => array($source_bundle_name)));
            $plugins[$plugin_id] = $plugin;

            if (!$relation->directional) {
              continue;
            }

            // Add the reverse relation in case of a directional relationship.
            $name = $relation_name . ':' . $target_entity_name . ':' . $target_bundle_name . ':' . $source_entity_name . ':' . $source_bundle_name . ':1';
            $plugin_id = $parent . ':' . $name;

            $replacements = array(
              '@source_entity' => $source_entity['label'],
              '@target_entity' => $target_entity['label'],
              '@source_bundle' => $source_entity['bundles'][$source_bundle_name]['label'],
              '@target_bundle' => $target_entity['bundles'][$target_bundle_name]['label'],
              '@relation' => $relation->reverse_label,
            );

            $plugin = $parent_plugin;
            $plugin['title'] = t('From @target_bundle (@target_entity) to @source_bundle (@source_entity) via relation @relation', $replacements);
            $plugin['keyword'] = $relation_name;
            $plugin['context name'] = $name . '-reverse';
            $plugin['name'] = $plugin_id;
            $plugin['description'] = t('Creates a @source_bundle context from @target_bundle using the @relation relation.', $replacements);
            $plugin['from entity'] = $target_entity_name;
            $plugin['from bundle'] = $target_bundle_name;
            $plugin['to entity'] = $source_entity_name;
            $plugin['to bundle'] = $source_bundle_name;
            $plugin['relation'] = $relation_name;
            $plugin['parent'] = $parent;
            $plugin['required context'] = new ctools_context_required($target_entity['label'], $target_entity_name, array('type' => array($target_bundle_name)));
            $plugins[$plugin_id] = $plugin;
          }
        }
      }
    }
  }

  $cache[$cid] = $plugins;
  return $plugins;
}

/**
 * Return a new context based on an existing context.
 */
function relation_panels_ctools_relationship_endpoint_to_endpoint_context($context, $configuration) {
  list(, $relation_name, $from_entity_type, $from_entity_bundle, $to_entity_type, $to_entity_bundle, $index) = explode(':', $configuration['name']);

  $entity_info = entity_get_info($from_entity_type);
  if (empty($context->data) || !isset($context->data->{$entity_info['entity keys']['id']})) {
    return ctools_context_create_empty('entity:' . $to_entity_type, NULL);
  }

  if (isset($context->data->{$entity_info['entity keys']['id']})) {
    list($id, , $bundle) = entity_extract_ids($from_entity_type, $context->data);

    if ($bundle == $from_entity_bundle) {
      if ($entity = relation_get_related_entity($from_entity_type, $id, $relation_name, $index)) {
        return ctools_context_create('entity:' . $to_entity_type, $entity);
      }
    }
  }

  return ctools_context_create_empty('entity:' . $to_entity_type, NULL);
}
