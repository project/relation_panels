<?php

/**
 * @file
 * Handle the 'relation view' override task.
 *
 * This plugin overrides relation/%relation and reroutes it to the page manager, where
 * a list of tasks can be used to service this request based upon criteria
 * supplied by access plugins.
 */

/**
 * Specialized implementation of hook_page_manager_task_tasks(). See api-task.html for
 * more information.
 */

/**
 * Plugin definition callback.
 */
function relation_panels_relation_view_page_manager_tasks() {
  return array(
    'task type' => 'page',
    'title' => t('Relation template'),
    'admin title' => t('Relation template'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for displaying relations at <em>relation/%relation</em>. If you add variants, you may use selection criteria such as relation type or language or user access to provide different views of relations. If no variant is selected, the default Drupal relation view will be used.'),
    'admin path' => 'relation/%relation',
    'hook menu alter' => 'relation_panels_relation_view_menu_alter',
    'handler type' => 'context',
    'get arguments' => 'relation_panels_relation_view_get_arguments',
    'get context placeholders' => 'relation_panels_relation_view_get_contexts',
    'disabled' => variable_get('relation_panels_relation_view_disabled', TRUE),
    'enable callback' => 'relation_panels_relation_view_enable',
  );
}

/**
 * Callback defined by relation_panels_relation_view_page_manager_tasks().
 *
 * Alter the relation view input so that relation view comes to us rather than the
 * normal relation view process.
 */
function relation_panels_relation_view_menu_alter(&$items, $task) {
  if (variable_get('relation_panels_relation_view_disabled', TRUE)) {
    return;
  }

  // Override the relation view handler for our purpose.
  $callback = $items['relation/%relation']['page callback'];
  if ($callback == 'relation_ui_page' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['relation/%relation']['page callback'] = 'relation_panels_relation_view_page';
    $items['relation/%relation']['file path'] = $task['path'];
    $items['relation/%relation']['file'] = $task['file'];
  }
  else {
    variable_set('relation_panels_relation_view_disabled', TRUE);
  }
}

/**
 * Entry point for our overridden relation view.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through to Drupal core's
 * relation view, which is relation_page_view().
 */
function relation_panels_relation_view_page($relation) {
  // Load my task plugin
  $task = page_manager_get_task('relation_view');

  // Load the relation into a context.
  ctools_include('context');
  ctools_include('context-task-handler');

  // We need to mimic Drupal's behavior of setting the relation title here.
  drupal_set_title(entity_ui_get_page_title('view', 'relation', $relation));
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($relation));
  $output = ctools_context_handler_render($task, '', $contexts, array($relation->rid));
  if ($output != FALSE) {
    return $output;
  }

  $function = 'relation_ui_page';
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('relation_view')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }

  // Otherwise, fall back.
  return $function($relation);
}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the relation view and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function relation_panels_relation_view_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'relation',
      'identifier' => t('relation being viewed'),
      'id' => 1,
      'name' => 'entity_id:relation',
      'settings' => array(),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function relation_panels_relation_view_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(relation_panels_relation_view_get_arguments($task, $subtask_id));
}

/**
 * Callback to enable/disable the page from the UI.
 */
function relation_panels_relation_view_enable($cache, $status) {
  variable_set('relation_panels_relation_view_disabled', $status);
}
