<?php
/**
 * @file
 * Definition of the relation plugin.
 */

$plugin = array(
  'handler' => array(
    'class' => 'PanelizerEntityRelation',
    'file' => 'PanelizerEntityRelation.php',
    'path' => drupal_get_path('module', 'relation_panels') . '/includes',
  ),
  'entity path' => 'relation/%relation',
  'uses page manager' => TRUE,
  'hooks' => array(
    'menu' => TRUE,
    'admin_paths' => TRUE,
    'permission' => TRUE,
    'panelizer_defaults' => TRUE,
    'default_page_manager_handlers' => TRUE,
    'form_alter' => TRUE,
    'views_data_alter' => TRUE,
  ),
);
